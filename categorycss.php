<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Outputs category custom css after processing it through the standard
 * Moodle processing and optimizations
 *
 * @copyright 2014 University of Wisconsin - Madison, UW Board of Regents
 * @author  John Hoopes <hoopes@wisc.edu>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package theme-big_red_responsive
 */

// disable moodle specific debug messages and any errors in output,
// comment out when debugging or better look into error log!
define('NO_DEBUG_DISPLAY', true);
define('NO_MOODLE_COOKIES', true);

require_once('../../config.php');

$catid = optional_param('catid', '', PARAM_ALPHANUMEXT);   // category idnumber e.g. "B"
$file = optional_param('file', '', PARAM_FILE); // file name e.g. "main.css"
$lifetime  = 60*60*4;                                   // Seconds to cache this stylesheet (4 hours)

$PAGE->set_url('/theme/big_red_responsive/categorycss.php', array('catid'=>$catid, 'file'=>$file));

$filepath = $CFG->dirroot . '/theme/big_red_responsive/org/' . $catid . '/' . $file;
if (file_exists($filepath)) {

    // determine whether or not to send caching headers
    $themerev = theme_get_revision();
    $theme = theme_config::load('big_red_responsive');

    // get CSS contents, process, and minify it to set content lengths
    $css = file_get_contents($filepath);
    $css = $theme->post_process($css);
    $css = core_minify::css($css);

    if($themerev <= 0){ // meaning theme designer mode is on, don't send caching headers

        header('Last-Modified: '. gmdate('D, d M Y H:i:s', time()) .' GMT');
        header('Expires: '. gmdate('D, d M Y H:i:s', time() + THEME_DESIGNER_CACHE_LIFETIME) .' GMT');
    }else{
        $etag = sha1($themerev . '/' . 'big_red_responsive');

        header('Last-Modified: '. gmdate('D, d M Y H:i:s', filemtime($filepath)) .' GMT');
        header('Expires: '. gmdate('D, d M Y H:i:s', time() + $lifetime) .' GMT');
        header('Etag: "'.$etag.'"');
        header('Cache-Control: public, max-age='.$lifetime);
    }
    header('Content-Disposition: inline; filename="'.$file.'"');
    header('Pragma: ');
    header('Accept-Ranges: none');
    header('Content-Type: text/css; charset=utf-8');
    header('Content-Length: '.strlen($css));

    echo $css;
}