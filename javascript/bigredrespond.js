// We need to actually use the code manually here as this is tricky do in
// themes at present.
YUI().use('moodle-theme_big_red_responsive-respond', function(Y) {
    Y.Moodle.theme_big_red_responsive.respond.init();
});
