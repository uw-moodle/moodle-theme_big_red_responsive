M.theme_big_red_responsive = M.theme_big_red_responsive || {};
NS = M.theme_big_red_responsive.sitenotice = {};

NS.init = function(Y, now) {
    this.now = now;
    Y.delegate('click', this.handle_expand, Y.config.doc, '#bgr-sitenotice .noticeheader a', this);
    Y.delegate('click', this.handle_dismiss, Y.config.doc, '#bgr-sitenotice .noticecontent a.dismiss', this);
};

NS.handle_expand = function(e) {
    var content = Y.one('#bgr-sitenotice .noticecontent');
    if (content.hasClass('content-hidden')) {
        content.replaceClass('content-hidden', 'content-visible');
    } else {
        content.replaceClass('content-visible', 'content-hidden');
    }
};

NS.handle_dismiss = function(e) {
    var notice = Y.one('#bgr-sitenotice');
    M.util.set_user_preference("bgr-sitenotice-confirm", this.now);
    notice.setStyle('display', 'none');
};
