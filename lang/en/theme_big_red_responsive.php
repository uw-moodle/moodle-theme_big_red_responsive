<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_big_red_responsive', language 'en'
 *
 * @package   theme_big_red_responsive
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['choosereadme'] = '
<div class="clearfix">
<div class="well">
<h2>Clean</h2>
<p><img class=img-polaroid src="theme/big_red_responsive/pix/screenshot.jpg" /></p>
</div>
<div class="well">
<h3>About</h3>
<p>Big Red Responsive is a theme that was cloned from the clean theme to then be changed to match Wisconsin colors.  It also
    has a few changes to the core rendering and some changes to the layouts to help for more content space at different screen
    sizes.</p>
<h3>Parents</h3>
<p>This theme is based upon the Clean theme, which was created for Moodle 2.5, with the help of:<br>
Bas Brands, David Scotson, Mary Evans.</p>
<p>This theme is based upon the Bootstrap theme, which was created for Moodle 2.5, with the help of:<br>
Stuart Lamour, Mark Aberdour, Paul Hibbitts, Mary Evans.</p>
<h3>Theme Credits</h3>
<p>Authors: John Hoopes<br>
Contact: hoopes@wisc.edu<br>
Website: <a href="https://courses.moodle.wisc.edu">courses.moodle.wisc.edu</a>
</p>
<h3>Report a bug:</h3>
<p>Email: <a href="mailto:uwmm-request@engr.wisc.edu">uwmm-request@engr.wisc.edu</a></p>
<h3>More information</h3>
<p><a href="http://kb.wisc.edu">kb.wisc.edu</a></p>
</div></div>';

$string['configtitle'] = 'Big red responsive';

$string['general'] = 'General';

$string['customcss'] = 'Custom CSS';
$string['customcssdesc'] = 'Whatever CSS rules you add to this textarea will be reflected in every page, making for easier customization of this theme.';

$string['footnote'] = 'Footnote';
$string['footnotedesc'] = 'Whatever you add to this textarea will be displayed in the footer throughout your Moodle site.';

$string['invert'] = 'Invert navbar';
$string['invertdesc'] = 'Swaps text and background for the navbar at the top of the page between black and white.';

$string['logo'] = 'Logo';
$string['logodesc'] = 'Please upload your custom logo here if you want to add it to the header.<br>
If the height of your logo is more than 75px add the following CSS rule to the Custom CSS box below.<br>
a.logo {height: 100px;} or whatever height in pixels the logo is.';

$string['pluginname'] = 'Big Red Responsive';

$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';

$string['course_home'] = 'Course home';
$string['home'] = 'home';


// Custom theme setting strings
$string['custom_theme_settings'] = 'Custom theme settings';

$string['secondary_menubar_login_setting'] = 'Enable secondary menu bar links';
$string['secondary_menubar_login_setting_desc'] = 'This setting removes the standard login text and information at the top right ' .
                                                    'and displays a help link with just login/logout text';

$string['help_link_setting'] = 'Help link url';
$string['help_link_setting_desc'] = 'Help link url for the help link displayed in the menu bar if secondary menubar is enabled';

$string['visibility_reminder_setting'] = 'Show course visibility message';
$string['visibility_reminder_setting_desc'] = 'Shows a message at the top of the course page to let instructors know their course isn\'t visible';
$string['visibilityremindertext'] = 'REMINDER: This course is currently hidden. Change the <a href="{$a->editlink}" title="Edit settings" style="text-decoration:underline;" >course visibility setting</a> when you are ready.';

$string['netid_login_setting'] = 'Enable front page login box';
$string['netid_login_setting_desc'] = 'This will enable a login options box on the front page for users not logged in';

$string['fpl_netid_button_text'] = 'Front page login NetID button text';
$string['fpl_netid_button_text_desc'] = 'This is the text that appears in the button for the NetID login.';

$string['fpl_local_login_button_text'] = 'Front page login visitor login button text';
$string['fpl_local_login_button_text_desc'] = 'This is the text that appears in the button for the Local login.';

$string['fpl_help_link'] = 'Frong page login help link';
$string['fpl_help_link_desc'] = 'This is the link that gets used for when a user needs help for logging in.';


$string['loggedinasguest'] = 'Logged in as guest';
$string['guestuserlogin'] = 'Login as full user';

// Site selector strings
$string['site_selector'] = 'Site selector';
$string['site_selector_setting'] = 'Enable Moodle site selector';
$string['site_selector_setting_desc'] = 'This will enable a select form element that will transfer users to links you specify';
$string['site_selector_label_setting'] = 'Moodle site selector select label';
$string['site_selector_label_setting_desc'] = 'The label that will appear next to the select element';
$string['site_selector_links'] = 'Site Selector Links';
$string['site_selector_links_desc'] = '<p>This text area is where you specify the site options available to the user.</p>' .
                                      '<p>Each option must be on its own line.  And each option has the site URL first, separated ' .
                                        'by a double colon, followed by the text that will show up in the site selector element</p>' .
                                      '<p>E.g. <br />https://courses.moodle.wisc.edu/prod/my::UW Moodle Courses</p>';

$string['site_selector_redirect'] = 'You are now redirecting to {$a->name} ({$a->url}) in 3 seconds';
$string['invalidsiteselectorlink'] = 'Invalid link.  If you have reached this error, please try again or contact support.';

// Logo Text settings
$string['logo_text_line1'] = 'University of Wisconsin Moodle'; // Default line 1
$string['logo_text_line1_setting'] = 'Logo line 1 text';
$string['logo_text_line1_setting_desc'] = 'This line goes in the first line of a 2 line logo.  This is what appears on desktops ' .
                                            'and tablets';
$string['logo_text_line2_setting'] = 'Logo line 2 text';
$string['logo_text_line2_setting_desc'] = 'This line goes in the second line of a 2 line logo.  This is what appears on desktops ' .
                                            'and tablets';

$string['mobile_logo_text_setting'] = 'Mobile logo text';
$string['mobile_logo_text_setting_desc'] = 'This text appears for Mobile devices.  Generally this should be short as phones don\'t ' .
                                            'have a lot of real-estate for text and images';
$string['logo_path_setting'] = 'Specify logo path';
$string['logo_path_setting_desc'] = 'Use this setting for when you\'d like to specify a local moodle path to the image instead of ' .
                                        'uploading it in the box above (NOTE: If this setting, anything above will be ignored)';

$string['closewindow'] = 'Close window';

$string['someallowguest'] = 'Guest user login';
$string['someallowguest_help'] = 'This will log you in as a guest user. Some courses may allow guest access';

$string['sitenoticeheader'] = 'Site notice header';
$string['sitenoticeheaderdesc'] = '';
$string['sitenoticebody'] = 'Site notice body';
$string['sitenoticebodydesc'] = '';
$string['sitenoticeremind'] = 'Site notice reminders';
$string['sitenoticereminddesc'] = 'How many days between site notice reminders.  0 for no reminders';
$string['sitenoticehideuntil'] = 'Click here to hide this message for {$a} days.';
$string['sitenoticehide'] = 'Click here to hide this message.';

