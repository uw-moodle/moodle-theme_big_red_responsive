<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$hostparts = explode('.', $_SERVER['HTTP_HOST']);

if($hostparts == 'www'){
    $host = $hostparts[1];
}else{
    $host = $hostparts[0];
}

if(is_dir($CFG->dirroot . '/theme/big_red_responsive/pix/app-icons/' . $host)){
    $appiconpath = $CFG->wwwroot . '/theme/big_red_responsive/pix/app-icons/' . $host . '/';
}else{
    $appiconpath = $CFG->wwwroot . '/theme/big_red_responsive/pix/app-icons/default/';
}

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="<?php echo $PAGE->title; ?>">
    <link rel="apple-touch-icon" href="<?php echo $appiconpath; ?>apple-touch-icon-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo $appiconpath; ?>apple-touch-icon-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo $appiconpath; ?>touch-icon-ipad-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo $appiconpath; ?>touch-icon-iphone-retina-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo $appiconpath; ?>touch-icon-ipad-retina-precomposed.png">
    <meta name="msapplication-TileColor" content=" #b70101" />
    <meta name="msapplication-square70x70logo" content="<?php echo $appiconpath; ?>touch-icon-ipad-precomposed.png" />
    <meta name="msapplication-square150x150logo" content="<?php echo $appiconpath; ?>touch-icon-ipad-retina-precomposed.png" />
    <meta name="msapplication-wide310x150logo" content="<?php echo $appiconpath; ?>310x150.png" />
    <meta name="msapplication-square310x310logo" content="<?php echo $appiconpath; ?>310x310.png" />
</head>

<body <?php echo $OUTPUT->body_attributes(); ?>>
<?php echo $OUTPUT->standard_top_of_body_html() ?>
<div id="page">
    <div id="page-content" class="clearfix">
        <?php echo $OUTPUT->main_content(); ?>
    </div>
</div>
<?php echo $OUTPUT->standard_end_of_body_html() ?>
</body>
</html>