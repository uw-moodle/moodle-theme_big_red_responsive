<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Big Red Responsive, an example of how to make a Bootstrap theme
 *
 *
 * For full information about creating Moodle themes, see:
 * http://docs.moodle.org/dev/Themes_2.0
 *
 * @package   theme_big_red_responsive
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
global $CFG, $OUTPUT;
// Get the HTML for the settings bits.
$html = theme_big_red_responsive_get_html_for_settings($OUTPUT, $PAGE);

$hostparts = explode('.', $_SERVER['HTTP_HOST']);

if($hostparts == 'www'){
    $host = $hostparts[1];
}else{
    $host = $hostparts[0];
}

if(is_dir($CFG->dirroot . '/theme/big_red_responsive/pix/app-icons/' . $host)){
    $appiconpath = $CFG->wwwroot . '/theme/big_red_responsive/pix/app-icons/' . $host . '/';
}else{
    $appiconpath = $CFG->wwwroot . '/theme/big_red_responsive/pix/app-icons/default/';
}

if (right_to_left()) {
    $regionbsid = 'region-bs-main-and-post';
} else {
    $regionbsid = 'region-bs-main-and-pre';
}

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="<?php echo $PAGE->title; ?>">
    <link rel="apple-touch-icon" href="<?php echo $appiconpath; ?>apple-touch-icon-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo $appiconpath; ?>apple-touch-icon-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo $appiconpath; ?>touch-icon-ipad-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo $appiconpath; ?>touch-icon-iphone-retina-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo $appiconpath; ?>touch-icon-ipad-retina-precomposed.png">
    <meta name="msapplication-TileColor" content=" #b70101" />
    <meta name="msapplication-square70x70logo" content="<?php echo $appiconpath; ?>touch-icon-ipad-precomposed.png" />
    <meta name="msapplication-square150x150logo" content="<?php echo $appiconpath; ?>touch-icon-ipad-retina-precomposed.png" />
    <meta name="msapplication-wide310x150logo" content="<?php echo $appiconpath; ?>310x150.png" />
    <meta name="msapplication-square310x310logo" content="<?php echo $appiconpath; ?>310x310.png" />
</head>

<body <?php echo $OUTPUT->body_attributes(); ?>>

<?php echo $OUTPUT->standard_top_of_body_html() ?>

<header role="banner" id="floating-banner" class="navbar navbar-fixed-top<?php echo $html->navbarclass ?> moodle-has-zindex">
    <nav role="navigation" class="navbar-inner">
        <div class="container-fluid bgr-responsive-container">
            <?php echo $html->heading; ?>
            <a class="btn btn-navbar" data-toggle="workaround-collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <?php echo $OUTPUT->user_menu() ?>
            <div class="nav-collapse collapse">
                <div class="visible-phone">
                    <?php echo $OUTPUT->custom_menu(); ?>
                </div>
                <ul class="nav pull-right">
                    <?php
                        $pageheadingmenu = $OUTPUT->page_heading_menu();
                        if(!empty($pageheadingmenu)) {
                            ?>
                            <li><?php echo $pageheadingmenu ?></li>
                        <?php
                        }
                    ?>
                    <li><?php echo big_red_responsive_get_site_selector(); ?></li>
                    <?php echo big_red_responsive_get_navbar_user_menu(); ?>
                </ul>
            </div>
        </div>
    </nav>
</header>

<div id="page" class="container-fluid">

    <header id="page-header" class="clearfix">
        <div id="page-navbar" class="clearfix">
            <nav class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></nav>
            <div class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></div>
        </div>
        <div id="course-header">
            <?php echo $OUTPUT->course_header(); ?>
        </div>
    </header>

    <div id="page-content" class="row-fluid">
        <div id="<?php echo $regionbsid ?>" class="span9 bgr-tablet-landscape-full">
            <div class="row-fluid">
                <section id="region-main" class="span9 pull-right">
                    <?php
                    echo big_red_responsive_get_login_block('mobile', true);
                    echo $OUTPUT->course_content_header();
                    echo $OUTPUT->main_content();
                    echo $OUTPUT->course_content_footer();
                    ?>
                </section>
                <div class="span3 desktop-first-column">
                    <?php
                        echo big_red_responsive_get_login_block('desktop', true);
                        echo $OUTPUT->blocks('side-pre', '');
                    ?>
                </div>
            </div>
        </div>
        <?php echo $OUTPUT->blocks('side-post', 'span3'); ?>
    </div>

    <footer id="page-footer">
        <div id="course-footer"><?php echo $OUTPUT->course_footer(); ?></div>
        <p class="helplink"><?php echo $OUTPUT->page_doc_link(); ?></p>
        <?php
        echo $html->footnote;
        echo $OUTPUT->login_info();
        echo $OUTPUT->home_link();
        echo $OUTPUT->standard_footer_html();
        ?>
    </footer>

    <?php echo $OUTPUT->standard_end_of_body_html() ?>

</div>
</body>
</html>
