<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Moodle's Clean theme, an example of how to make a Bootstrap theme
 *
 * DO NOT MODIFY THIS THEME!
 * COPY IT FIRST, THEN RENAME THE COPY AND MODIFY IT INSTEAD.
 *
 * For full information about creating Moodle themes, see:
 * http://docs.moodle.org/dev/Themes_2.0
 *
 * @package   theme_big_red_responsive
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Parses CSS before it is cached.
 *
 * This function can make alterations and replace patterns within the CSS.
 *
 * @param string $css The CSS
 * @param theme_config $theme The theme config object.
 * @return string The parsed CSS The parsed CSS.
 */
function theme_big_red_responsive_process_css($css, $theme) {

    // Set the background image for the logo.
    $logo = $theme->setting_file_url('logo', 'logo');
    $css = theme_big_red_responsive_set_logo($css, $logo);

    // Set custom CSS.
    if (!empty($theme->settings->customcss)) {
        $customcss = $theme->settings->customcss;
    } else {
        $customcss = null;
    }
    $css = theme_big_red_responsive_set_customcss($css, $customcss);

    return $css;
}

/**
 * Adds the logo to CSS.
 *
 * @param string $css The CSS.
 * @param string $logo The URL of the logo.
 * @return string The parsed CSS
 */
function theme_big_red_responsive_set_logo($css, $logo) {
    $tag = '[[setting:logo]]';
    $replacement = $logo;

    if (is_null($replacement)) {
        // if the logo is still null, put in the default UW Crest
        $logourl = new moodle_url('/theme/big_red_responsive/pix/uwcrest.png');
        $replacement = $logourl->out();
    }

    $css = str_replace($tag, $replacement, $css);

    return $css;
}

/**
 * Serves any files associated with the theme settings.
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param context $context
 * @param string $filearea
 * @param array $args
 * @param bool $forcedownload
 * @param array $options
 * @return bool
 */
function theme_big_red_responsive_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = array()) {
    if ($context->contextlevel == CONTEXT_SYSTEM and $filearea === 'logo') {
        $theme = theme_config::load('big_red_responsive');
        return $theme->setting_file_serve('logo', $args, $forcedownload, $options);
    } else {
        send_file_not_found();
    }
}

/**
 * Adds any custom CSS to the CSS before it is cached.
 *
 * @param string $css The original CSS.
 * @param string $customcss The custom CSS to add.
 * @return string The CSS which now contains our custom CSS.
 */
function theme_big_red_responsive_set_customcss($css, $customcss) {
    $tag = '[[setting:customcss]]';
    $replacement = $customcss;
    if (is_null($replacement)) {
        $replacement = '';
    }

    $css = str_replace($tag, $replacement, $css);

    return $css;
}

/**
 * Returns an object containing HTML for the areas affected by settings.
 *
 * @param renderer_base $output Pass in $OUTPUT.
 * @param moodle_page $page Pass in $PAGE.
 * @return stdClass An object with the following properties:
 *      - navbarclass A CSS class to use on the navbar. By default ''.
 *      - heading HTML to use for the heading. A logo if one is selected or the default heading.
 *      - footnote HTML to use as a footnote. By default ''.
 */
function theme_big_red_responsive_get_html_for_settings(renderer_base $output, moodle_page $page) {
    global $CFG;
    $return = new stdClass;

    $return->navbarclass = '';
    if (!empty($page->theme->settings->invert)) {
        $return->navbarclass .= ' navbar-inverse';
    }

    if (!empty($page->theme->settings->logo)) {

    }

    if(!during_initial_install()){
        //$return->heading = html_writer::link($CFG->wwwroot, '', array('title' => get_string('home'), 'class' => 'logo'));

        $bgrconfig = get_config('theme_big_red_responsive');
        if(empty($bgrconfig->logotextl1)){
            $bgrconfig->logotextl1 = get_string('logo_text_line1', 'theme_big_red_responsive');
        }

        if(empty($bgrconfig->logotextl2)){
            $bgrconfig->logotextl2 = '';
        }
        if(empty($bgrconfig->mlogotext)){
            $bgrconfig->mlogotext = '';
        }
    }else{
        $bgrconfig = new stdClass();
        $bgrconfig->logotextl1 = get_string('logo_text_line1', 'theme_big_red_responsive');
        $bgrconfig->logotextl2 = '';
        $bgrconfig->mlogotext = '';
    }

    $logo = html_writer::div('', 'logo', array('title'=> get_string('home')));

    $t = html_writer::div(
        html_writer::span($bgrconfig->logotextl1 . '<br />' . $bgrconfig->logotextl2, 'logo-text'),
        'hidden-phone');
    $t2 = html_writer::div(
        html_writer::span($bgrconfig->mlogotext, 'visible-phone logo-text logo-text-mobile'),
        'hidden-desktop');

    $text = html_writer::div($t . $t2, 'logo-text-container');
    $container = html_writer::div($logo . $text, 'pull-left logo-container');
    $container = html_writer::link($CFG->wwwroot, $container, array('title' => get_string('home')));
    $return->heading = $container;


    $return->footnote = '';
    if (!empty($page->theme->settings->footnote)) {
        $return->footnote = '<div class="footnote text-center">'.$page->theme->settings->footnote.'</div>';
    }

    return $return;
}

/**
 * All theme functions should start with theme_big_red_responsive_
 * @deprecated since 2.5.1
 */
function big_red_responsive_process_css() {
    throw new coding_exception('Please call theme_'.__FUNCTION__.' instead of '.__FUNCTION__);
}

/**
 * All theme functions should start with theme_big_red_responsive_
 * @deprecated since 2.5.1
 */
function big_red_responsive_set_logo() {
    throw new coding_exception('Please call theme_'.__FUNCTION__.' instead of '.__FUNCTION__);
}

/**
 * All theme functions should start with theme_big_red_responsive_
 * @deprecated since 2.5.1
 */
function big_red_responsive_set_customcss() {
    throw new coding_exception('Please call theme_'.__FUNCTION__.' instead of '.__FUNCTION__);
}

/**
 * Gets and builds the site selector if it's turned on and has available site links.
 *
 * @return string HTML representation of the site selector
 */
function big_red_responsive_get_site_selector(){
    global $OUTPUT, $CFG;
    $html = '';

    $siteselectorenabled = get_config('theme_big_red_responsive', 'enablesiteselector');

    if((int)$siteselectorenabled === 1){

        // get selector links and explode on line break to get individual options
        $siteselectorlinks = get_config('theme_big_red_responsive', 'siteselectorlinks');

        if(!empty($siteselectorlinks)){

            // replace any \r\n with \n to make sure all types of line breaks are the same
            $siteselectorlinks = str_replace("\r\n", "\n", $siteselectorlinks);
            // Get each option specified in the text area
            $linkopts = explode("\n", $siteselectorlinks);

            // The value of the option is the index of the option in the text area.
            //  This is to protect the siteselector script from becoming a redirect XSS attack
            //  The siteselector script will take this index and get that index from the siteselectorlinks config
            $options = array();
            $index = 0;
            foreach($linkopts as$opt){
                $optparts = explode("::", $opt);
                if(strstr($optparts[0], $CFG->wwwroot) ){
                    continue; // don't add the site url to the list of options if present
                }
                if(empty($optparts[0]) || empty($optparts[1])){
                    continue; // don't add if one of the parts is empty
                }

                $options[$optparts[0]] = $optparts[1];
                $index++;
            }
            $url = new moodle_url('/theme/big_red_responsive/siteselector.php');
            $siteselector = new single_select($url, 'site_selector_input', $options, '', array('' => 'choosedots'), 'site_selector');

            // Get selector label
            $labelsetting = get_config('theme_big_red_responsive', 'siteselectorlabel');
            if($labelsetting){
                $siteselector->set_label($labelsetting, array('id'=>'site_selector_label'));
            }else{
                $siteselector->set_label(get_string('site_selector', 'theme_big_red_responsive'), array('id'=>'site_selector_label'));
            }

            $html = $OUTPUT->render($siteselector);
            $html = html_writer::span($html, 'site-selector-container');
        }
    }

    return $html;
}

/**
 * Function to handle displaying the login block within layouts
 *
 * @param string $context The context in which to display (mobile/desktop)
 * @param bool $frontpage Whether or not we're displaying front page
 *
 * @return string HTML string of the login block, could be nothing if conditions aren't met
 */
function big_red_responsive_get_login_block($context, $frontpage = false){
    global $PAGE;

    $renderer = $PAGE->get_renderer('theme_big_red_responsive');

    $enablefrontpagelogin = get_config('theme_big_red_responsive', 'enablenetidlogin');
    if(!isloggedin() && !isguestuser() && $enablefrontpagelogin) {

        return $renderer->render_login_block($context, $frontpage);
    }else{
        return '';
    }
}


/**
 * Display the site notice
 *
 * @param string $context The context in which to display (mobile/desktop)
 * @param bool $frontpage Whether or not we're displaying front page
 *
 * @return string HTML string of the login block, could be nothing if conditions aren't met
 */
function big_red_responsive_get_site_notice($context, $frontpage = false){
    global $PAGE;

    $renderer = $PAGE->get_renderer('theme_big_red_responsive');

    return $renderer->render_site_notice();
}


/**
 * Prints the menubar login
 * This is configured with the theme settings
 *
 * @return string
 */
function big_red_responsive_get_navbar_user_menu(){
    global $OUTPUT, $CFG;


    $menubarlinks = '';

    // Add help link
    $helplink = get_config('theme_big_red_responsive', 'helplink');
    $menubarlinks .= '<li class=""><a href="' . $helplink . '">' . get_string('help') . '</a></li>';
    /*
    if(isloggedin() && !isguestuser()){ // if logged in get logout url
        $logouturl = $CFG->wwwroot . '/login/logout.php?sesskey=' . sesskey();
        $menubarlinks .= '<li class=""><a href="' . $logouturl . '">' . get_string('logout') . '</a></li>';
    }else if(isguestuser()) {

        $loginurl = get_login_url();
        $guestlogintext = get_string('loggedinasguest', 'theme_big_red_responsive') .
                        '<br /><a href="' . $loginurl . '">' . get_string('guestuserlogin', 'theme_big_red_responsive') .
                        '</a>';
        $guestlogintext = html_writer::span($guestlogintext, 'guestuserloginarea');

        $menubarlinks .= '<li class="">' . $guestlogintext . '</li>';
    }else{
        $loginurl = get_login_url();
        $menubarlinks .= '<li class=""><a href="' . $loginurl .'">' .get_string('login') .'</a></li>';
    }*/
    return $menubarlinks;

}


/**
 * This function is built to check a string to return a url to a custom css/js file
 * It's used in conjunction with custom css/js for categories
 *
 * @param string $string
 * @param string $type type of file (js or css)
 * @param string $catid categoryid for the local url building
 *
 * @return moodle_url|bool a moodle_url object if valid, or false if not valid
 */
function big_red_responsive_validate_url($string, $type = '', $catid = ''){
    global $CFG, $COURSE;

    // if it is already a URL return it
    if(filter_var($string, FILTER_VALIDATE_URL) && substr($string, 0, 4) == 'http'){
        // external url needs to be converted to moodle url object for requires css/js
        $url = new moodle_url($string);
        return $url;
    }
    // next try to see if the file exists locally by matching path with the organization's category id number
    if(file_exists($CFG->dirroot . '/theme/big_red_responsive/org/' . $catid . '/' . $string)){

        // next get the file type to build the right callback url
        switch($type){
            case 'css':
                $callback = new moodle_url('/theme/big_red_responsive/categorycss.php');
                $callback->param('catid', $catid);
                $callback->param('file', $string);
                return $callback;
                break;
            case 'js':
                $callback = new moodle_url('/theme/big_red_responsive/categoryjs.php');
                $callback->param('catid', $catid);
                $callback->param('file', $string);

                return $callback;
                break;
            default:
                // if no type just return false as we can't build the right callback file
                return false;
        }
    }
    add_to_log($COURSE->id, 0, 'addcustomcss', $string, 'URL did not validate');
    return false;
}

// Add in code for loading custom css and javascript
global $PAGE, $COURSE, $SITE, $CFG;
if(!is_null($COURSE) && $COURSE->id != $SITE->id){
    $reversedcategories = array_reverse($PAGE->categories);
    foreach($reversedcategories as $category){ // loop through the categories to see if there is config items for the category

        if(isset($CFG->bgr_category_setting[$category->idnumber])){
            $setting = $CFG->bgr_category_setting[$category->idnumber];
            // Add custom css files
            foreach($setting['css'] as $cssfile){
                if($url = big_red_responsive_validate_url($cssfile, 'css', $category->idnumber)){
                    $PAGE->requires->css($url);
                }
            }
            // Add custom js files
            foreach($setting['js'] as $jsfile){
                if($url = big_red_responsive_validate_url($jsfile, 'js', $category->idnumber)){
                    $PAGE->requires->js($url);
                }
            }
        }
    }
}


