// <![CDATA[
// Show/hide content & change background image
function toggleContent(contentId, bannerId){
    var text = document.getElementById(contentId);
    var banner = document.getElementById(bannerId);
    if (text.style["display"] == 'none') {
        text.style["display"] = 'block';
        banner.style["background"] = "#55565b";
    } else {
        text.style["display"] = 'none';
        banner.style["background"] = "#00577d";
    }
}

// Start with the columns closed
window.onload = function() {
    if(document.getElementById('course_description_banner') != null){
        document.getElementById("course_description_banner").onclick();
    }

    if(document.getElementById('outcomes_banner') != null){
        document.getElementById("outcomes_banner").onclick();
    }

    if(document.getElementById('activities_banner') != null) {
        document.getElementById("activities_banner").onclick();
    }
    if(document.getElementById('instructor_banner') != null) {
        document.getElementById("instructor_banner").onclick();
    }
};
// ]]>