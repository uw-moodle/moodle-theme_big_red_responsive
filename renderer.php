<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Custom render functions
 *
 * @package    theme
 * @subpackage big_red_responsive
 * @copyright  2014 University of Wisconsin - Madison
 * @author     John Hoopes <hoopes@wisc.edu>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

class theme_big_red_responsive_renderer extends plugin_renderer_base{


    /** @var stdClass $themeconfig Store the theme config for multiple
     *                              calls to functions in the renderer to save DB calls
     */
    protected $themeconfig;

    /**
     * Override parent construct to add the theme configuration
     *
     * @param moodle_page $page
     * @param string $target one of rendering target constants
     */
    public function __construct(moodle_page $page, $target){

        $this->themeconfig = get_config('theme_big_red_responsive');

        parent::__construct($page, $target);
    }

    /**
     * Renders the login block based on the provided context
     *
     * @param string $context Context in which to display the login block
     *
     * @return string
     */
    function render_login_block($context, $frontpage){
       global $CFG, $OUTPUT;

        switch($context){
            case 'desktop':
                $visibleclass = 'visible-desktop';
                break;
            case 'mobile':
                $visibleclass = 'hidden-desktop';
                break;
            default:
                $visibleclass = '';
                break;
        }

        $guesthelp = new help_icon('someallowguest', 'theme_big_red_responsive');
        $guesthelphtml = $OUTPUT->render($guesthelp);

        if ($CFG->guestloginbutton and !isguestuser()) {
            $guestlogin = '
                 <form action="' . $CFG->wwwroot . '/login/index.php" method="post" id="guestlogin">
                        <input type="hidden" name="username" value="guest">
                        <input type="hidden" name="password" value="guest">
                        <span class="buttonhelpline"><input type="submit" value="Log in as a guest"> ' . $guesthelphtml . '</span>
                </form>';
        }else{
            $guestlogin = '';
        }

        $netidbtn = '
            <p><a class="netidlink" rel="external"
                  href="' . $CFG->wwwroot . '/auth/shibboleth/index.php">
                  ' . $this->themeconfig->fplnetidbuttontext . '
            </a></p>
        ';

        $visitorlink = '
            <p><a class="visitorlink"
                  href="' . $CFG->wwwroot . '/local/wiscservices/login/index.php?local=1">
                  ' . $this->themeconfig->fpllocalloginbuttontext .'
            </a></p>
        ';

        // check for custom button layout
        if($frontpage && !empty($CFG->bgrfrontpageloginbtnlayout)){
            $btnlayout = explode(',', $CFG->bgrfrontpageloginbtnlayout);
        }else if(!empty($CFG->bgrloginbtnlayout)){
            $btnlayout = explode(',', $CFG->bgrloginbtnlayout);
        }else{
            $btnlayout = array('netid', 'visitorlink', 'guestlogin');
        }

        $buttons = '';
        foreach($btnlayout as $btnl){

            switch(trim($btnl)){
                case 'netid':
                    $buttons .= $netidbtn;
                    break;
                case 'visitorlink':
                    $buttons .= $visitorlink;
                    break;
                case 'guestlogin':
                    $buttons .= $guestlogin;
            }
        }

        if(!$frontpage){
            $helplink = '<span class="smallLink"> <a target="blank"
                                                        href="' . $this->themeconfig->fplhelplink . '">Which
                                    login method should I use?</a> </span>';
        }else{
            $helplink = '';
        }


        $html = '<div class="blockme ' . $visibleclass . '" id="front-page-login">
                    <div class="logincontainer">
                        <div class="login">
                            <p>Choose your login method:</p>

                            <div class="buttons">

                                ' . $buttons . '

                            </div>
                            ' . $helplink . '
                        </div>
                    </div>
                </div>';

        return $html;

    }

    function render_site_notice() {
        global $PAGE, $SESSION;

        $noticeheader = get_config('theme_big_red_responsive', 'sitenoticeheader');
        $noticebody = get_config('theme_big_red_responsive', 'sitenoticebody');
        if (empty($noticeheader) || empty($noticebody)) {
            // No notice configured
            return '';
        }
        $userpref = 'bgr-sitenotice-confirm';
        user_preference_allow_ajax_update($userpref, PARAM_INT);
        $lastconfirmed = get_user_preferences($userpref, 0);
        
        $remindtime = get_config('theme_big_red_responsive', 'sitenoticeremind');
        if (($remindtime == 0 && $lastconfirmed > 0)
              || (time() - $lastconfirmed < $remindtime*60*60*24)) {
            // already confirmed
            return '';
        }

        if (!empty($SESSION->theme_big_red_responsive_noticeview)) {
            $views = $SESSION->theme_big_red_responsive_noticeview;
        } else {
            $views = 0;
        }
        if ($views > 3) {
            // already viewed 3 times
            return '';
        }
        $SESSION->theme_big_red_responsive_noticeview = $views + 1;


        $PAGE->requires->js_init_call('M.theme_big_red_responsive.sitenotice.init', array(time()));

        if ($remindtime > 0) {
            $hidemessage = get_string('sitenoticehideuntil', 'theme_big_red_responsive', $remindtime);
        } else {
            $hidemessage = get_string('sitenoticehide', 'theme_big_red_responsive');
        }
        
        $iconurl = $this->pix_url('docs');

        $html = <<<EOF
            <div id="bgr-sitenotice">
                <div class="noticeheader">
                    <img src="$iconurl" style="vertical-align:top;" alt="reminder" />
                    <span class="caption line">$noticeheader <a href="javascript:void(0)"> <span class="line">Read more...</span></a>
                </div>
                <div class="noticecontent content-hidden">
                    $noticebody

                    <p>
                    <a class="dismiss" href="javascript:void(0)"> $hidemessage </a>
                    </p>
                </div>
            </div>
EOF;
        return $html;
    }
}
