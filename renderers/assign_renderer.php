<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

// require base theme renderer that then overrides the actual core_renderer
require_once($CFG->dirroot . '/mod/assign/renderer.php');

/**
 * Add core renderer functionality/override some functionality from base theme and core_renderer
 *
 * @package    theme_big_red_responsive
 * @copyright  2017, University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class theme_big_red_responsive_mod_assign_renderer extends mod_assign_renderer {

    /**
     * Override parent theme's custom menu to add our own menu items
     *
     * Parent function is copied so no need to call parent
     *
     * @param string $custommenuitems
     * @return string html representation of the menu
     */
    public function render_assign_grading_summary(assign_grading_summary $summary) {
        $reminder = '';
        if ($this->course_has_broken_formula()) {
            $reminder = $this->output->notification("The gradebook for this course may contain an invalid category calculation,  
                which can result in extremely slow performance during assignment grading.  <br><br><b>Please check the course gradebook for any 'Error' values and
                update the gradebook settings accordingly.</b>", 'notifyproblem');
        }
        return $reminder.parent::render_assign_grading_summary($summary);
    }

    public function course_has_broken_formula() {
        global $DB, $COURSE;
        $select = "courseid=? AND needsupdate=1 AND calculation IS NOT NULL";
        return $DB->record_exists_select('grade_items', $select, array($COURSE->id));
    }

}
