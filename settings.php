<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Moodle's Clean theme, an example of how to make a Bootstrap theme
 *
 * DO NOT MODIFY THIS THEME!
 * COPY IT FIRST, THEN RENAME THE COPY AND MODIFY IT INSTEAD.
 *
 * For full information about creating Moodle themes, see:
 * http://docs.moodle.org/dev/Themes_2.0
 *
 * @package   theme_big_red_responsive
 * @copyright 2014 Moodle, moodle.org, University of Wisconsin - Madison
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {

    // General Theme Settings
    $name = 'theme_big_red/generalheading';
    $title = get_string('general','theme_big_red_responsive');
    $setting = new admin_setting_heading($name, $title, '');
    $settings->add($setting);

        // Logo file setting.
        $name = 'theme_big_red_responsive/logo';
        $title = get_string('logo','theme_big_red_responsive');
        $description = get_string('logodesc', 'theme_big_red_responsive');
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        // Moodle Title Line 1
        $name = 'theme_big_red_responsive/logotextl1';
        $title = get_string('logo_text_line1_setting', 'theme_big_red_responsive');
        $description = get_string('logo_text_line1_setting_desc', 'theme_big_red_responsive');
        $default = get_string('logo_text_line1', 'theme_big_red_responsive');
        $setting = new admin_setting_configtext($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        // Moodle Title Line 2
        $name = 'theme_big_red_responsive/logotextl2';
        $title = get_string('logo_text_line2_setting', 'theme_big_red_responsive');
        $description = get_string('logo_text_line2_setting_desc', 'theme_big_red_responsive');
        $default = '';
        $setting = new admin_setting_configtext($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        // Moodle Mobile Title Line
        $name = 'theme_big_red_responsive/mlogotext';
        $title = get_string('mobile_logo_text_setting', 'theme_big_red_responsive');
        $description = get_string('mobile_logo_text_setting_desc', 'theme_big_red_responsive');
        $default = '';
        $setting = new admin_setting_configtext($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        // Custom CSS file.
        $name = 'theme_big_red_responsive/customcss';
        $title = get_string('customcss', 'theme_big_red_responsive');
        $description = get_string('customcssdesc', 'theme_big_red_responsive');
        $default = '';
        $setting = new admin_setting_configtextarea($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        // Footnote setting.
        $name = 'theme_big_red_responsive/footnote';
        $title = get_string('footnote', 'theme_big_red_responsive');
        $description = get_string('footnotedesc', 'theme_big_red_responsive');
        $default = '';
        $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

    // Custom Theme Settings
    $name = 'theme_big_red/customthemesettingsheading';
    $title = get_string('custom_theme_settings','theme_big_red_responsive');
    $setting = new admin_setting_heading($name, $title, '');
    $settings->add($setting);

        // Enable course visibility reminder
        $name = 'theme_big_red_responsive/visibilityreminder';
        $title = get_string('visibility_reminder_setting', 'theme_big_red_responsive');
        $description = get_string('visibility_reminder_setting_desc', 'theme_big_red_responsive');
        $default = 0;
        $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        // Help link url for secondary menubar
        $name = 'theme_big_red_responsive/helplink';
        $title = get_string('help_link_setting', 'theme_big_red_responsive');
        $description = get_string('help_link_setting_desc', 'theme_big_red_responsive');
        $default = '';
        $setting = new admin_setting_configtext($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        // Enable Front page login
        $name = 'theme_big_red_responsive/enablenetidlogin';
        $title = get_string('netid_login_setting', 'theme_big_red_responsive');
        $description = get_string('netid_login_setting_desc', 'theme_big_red_responsive');
        $default = 0;
        $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        // Front Page Login NetID button text
        $name = 'theme_big_red_responsive/fplnetidbuttontext';
        $title = get_string('fpl_netid_button_text', 'theme_big_red_responsive');
        $description = get_string('fpl_netid_button_text_desc', 'theme_big_red_responsive');
        $default = 'UW NetID Login';
        $setting = new admin_setting_configtext($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        // Front Page Login Local Login button text
        $name = 'theme_big_red_responsive/fpllocalloginbuttontext';
        $title = get_string('fpl_local_login_button_text', 'theme_big_red_responsive');
        $description = get_string('fpl_local_login_button_text_desc', 'theme_big_red_responsive');
        $default = 'Visitor Login';
        $setting = new admin_setting_configtext($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        // Front Page Login Help Link
        $name = 'theme_big_red_responsive/fplhelplink';
        $title = get_string('fpl_help_link', 'theme_big_red_responsive');
        $description = get_string('fpl_help_link_desc', 'theme_big_red_responsive');
        $default = 'https://kb.wisc.edu/page.php?id=30448';
        $setting = new admin_setting_configtext($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        // site notice
        $name = 'theme_big_red_responsive/sitenoticeheader';
        $title = get_string('sitenoticeheader', 'theme_big_red_responsive');
        $description = get_string('sitenoticeheaderdesc', 'theme_big_red_responsive');
        $default = '';
        $setting = new admin_setting_configtext($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        // site notice
        $name = 'theme_big_red_responsive/sitenoticebody';
        $title = get_string('sitenoticebody', 'theme_big_red_responsive');
        $description = get_string('sitenoticebodydesc', 'theme_big_red_responsive');
        $default = '';
        $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        $name = 'theme_big_red_responsive/sitenoticeremind';
        $title = get_string('sitenoticeremind', 'theme_big_red_responsive');
        $description = get_string('sitenoticereminddesc', 'theme_big_red_responsive');
        $default = '';
        $setting = new admin_setting_configtext($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);


    // Site selector settings
    $name = 'theme_big_red/siteselectorheading';
    $title = get_string('site_selector','theme_big_red_responsive');
    $setting = new admin_setting_heading($name, $title, '');
    $settings->add($setting);

        // Enable Moodle site selector
        $name = 'theme_big_red_responsive/enablesiteselector';
        $title = get_string('site_selector_setting', 'theme_big_red_responsive');
        $description = get_string('site_selector_setting_desc', 'theme_big_red_responsive');
        $default = 0;
        $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        // Moodle Site selector label
        $name = 'theme_big_red_responsive/siteselectorlabel';
        $title = get_string('site_selector_label_setting', 'theme_big_red_responsive');
        $description = get_string('site_selector_label_setting_desc', 'theme_big_red_responsive');
        $default = get_string('site_selector', 'theme_big_red_responsive');
        $setting = new admin_setting_configtext($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        // Moodle Site selector links
        // text area with each line representing a link with the link itself first then "::" and then text in select box
        $name = 'theme_big_red_responsive/siteselectorlinks';
        $title = get_string('site_selector_links', 'theme_big_red_responsive');
        $description = get_string('site_selector_links_desc', 'theme_big_red_responsive');
        $default = '';
        $setting = new admin_setting_configtextarea($name, $title, $description, $default);
        $setting->get_id();
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

}
