<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Site selector script to redirect user for the value specified
 *
 * @package    theme_big_red_responsive
 * @author     John Hoopes (hoopes@wisc.edu)
 * @copyright  2014 University of Wisconsin - Madison
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// require config file
require_once("../../config.php");
require_once($CFG->libdir . '/weblib.php');


$selectedoption = required_param('site_selector_input', PARAM_URL);


// check to make sure that selected option is within the theme's settings
$siteselectorlinks = get_config('theme_big_red_responsive', 'siteselectorlinks');

// replace any \r\n with \n to make sure all types of line breaks are the same
$siteselectorlinks = str_replace("\r\n", "\n", $siteselectorlinks);
// Get each option specified in the text area
$linkopts = explode("\n", $siteselectorlinks);

// Attempt to match the selected option
$foundlink = false;
$link = array();
foreach($linkopts as$opt){
    $optparts = explode("::", $opt);
    if($optparts[0] == $selectedoption){
        $foundlink = true;
        $link = $optparts;
    }
}

// Redirect if link is found
if($foundlink){
    $url = new moodle_url($link[0]);
    $a = new stdClass();
    $a->url = $link[0];
    $a->name = $link[1];
    redirect($url, get_string('site_selector_redirect', 'theme_big_red_responsive', $a), 3);
}else{ // throw moodle error if not found

    throw new moodle_exception('invalidsiteselectorlink', 'theme_big_red_responsive');

}







