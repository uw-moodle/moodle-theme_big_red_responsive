<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Big Red Responsive theme
 *
 * This theme is based off of the bootstrap base theme as well as the clean theme
 *   provided by Moodle
 *
 * @package    theme_big_red_responsive
 * @copyright  2014 University of Wisconsin, UW Board of Regents
 * @author     John Hoopes
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

$plugin->version   = 2018011100;
$plugin->requires  = 2013110500;
$plugin->component = 'theme_big_red_responsive';
$plugin->dependencies = array(
    'theme_bootstrapbase'  => 2013110500,
);
