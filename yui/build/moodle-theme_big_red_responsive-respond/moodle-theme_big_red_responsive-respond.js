YUI.add('moodle-theme_big_red_responsive-respond', function (Y, NAME) {


M.theme_big_red_responsive = M.theme_big_red_responsive || {};
var NS = Y.namespace('Moodle.theme_big_red_responsive.respond');

NS.init = function(){
    var viewport = Y.one(window);

    if(window.innerWidth < 768){
        var navblock = Y.one('.block_navigation');
        var phonenavblock = Y.one('#phone-nav');

        phonenavblock.setHTML(navblock.get('outerHTML'));
        navblock.remove();
    }

    viewport.on('resize', function(){
        console.log('Window Changed');

        if(window.innerWidth > 768){

            var phonenavblock = Y.one('#phone-nav');

            var pnavblock = phonenavblock.one('.block_navigation');
            if(pnavblock){

                var sideregion = Y.one('#block-region-side-pre');
                sideregion.prepend(pnavblock);
                phonenavblock.setHTML('');
            }
        }else if(window.innerWidth < 768){
            var phonenavblock = Y.one('#phone-nav');

            var pnavblock = phonenavblock.one('.block_navigation');
            if(pnavblock === null){
                var navblock = Y.one('.block_navigation');

                phonenavblock.setHTML(navblock.get('outerHTML'));
                navblock.remove();
            }
        }
    });
};

}, '@VERSION@', {"requires": ["base", "node", "selector-css3", "event-base"]});
